package main

import (
	"calculator-go/initializers"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

func init() {
	initializers.LoadEnvVariables()
}

func HelloWorld(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "Hello World!",
	})
}

func QueryString(c *gin.Context) {
	firstValueQuery := c.Query("firstValue")
	operator := c.Query("operator")
	secondValueQuery := c.Query("secondValue")

	firstValue, errFirst := strconv.ParseFloat(firstValueQuery, 64)
	secondValue, errSecond := strconv.ParseFloat(secondValueQuery, 64)
	var results float64
	var response string

	switch {
	case operator == " " || operator == "addition":
		results = firstValue + secondValue
		response = "The results is " + fmt.Sprintf("%v", results)
	case operator == "-" || operator == "subtraction":
		results = firstValue - secondValue
		response = "The results is " + fmt.Sprintf("%v", results)
	case operator == "*" || operator == "multiply " || operator == "x":
		results = firstValue * secondValue
		response = "The results is " + fmt.Sprintf("%v", results)
	case operator == "/" || operator == "division" || operator == ":":
		results = firstValue / secondValue
		response = "The results is " + fmt.Sprintf("%v", results)
	default:
		response = "Please enter the operator with (+, -, *, /) or (addition, subtraction, multiply, division)"
	}

	if errFirst != nil {
		response = "Please enter the first value with a number"
	}
	if errSecond != nil {
		response = "Please enter the second value with a number"
	}
	if firstValueQuery == "" || secondValueQuery == "" {
		response = "Please enter the value"
	}

	c.JSON(200, gin.H{
		"message": response,
	})
}

func main() {
	r := gin.Default()
	r.GET("/", HelloWorld)
	r.GET("/query", QueryString)
	r.Run()
}
